![](https://i.imgur.com/bCKVeki.png)

# EstateCare
Como configurar o projeto no seu computador:

### Pré requisitos

Composer - https://getcomposer.org/ (preferencialmente versão 1.0)

PHP 7.4

Termianal

### Linux
1. A fim de inicializar o projeto em máquinas com o
sistema operacional Linux será também necessário possuir o software Docker instalado na sua máquina - https://www.docker.com/

2. Clone o projeto para a sua máquina:
```git clone https://gitlab.com/projeto-de-desenvolvimento/estatecare.git```

3. Navegue até a pasta raíz que foi clonada

4. Execute o comando ```docker-compose up``` (se você não deseja acompanhar a cosntrução do projeto você pode executar
o mesmo comando seguido do parâmetro ``` -d```, o qual deixará o terminal livre para interações)

5. Para visualizar os containers que estão rodando você pode utilizar o comando ```docker ps```

6. Se você deseja se conectar a um container utilize o comando ```docker exec -t 'nome do container'```

7. É recomendado rodar o o comando ```composer isntall``` dentro do container do projeto estatecare( acesse ```docker exec -t estatecare``` e rode ```composer isntall```)

8. Pronto, o projeto já deve estar disponível para você navegar e desenvolver na sua máquina, basta acessar a url ```localhost:8000``` no seu navegador

### Windows
1. A fim de inicializar o projeto em máquinas com o
sistema operacional Windows será também necessário possuir o software XAMPP instalado na sua máquina - https://www.apachefriends.org/index.html

2. Também é recomendado o terminal GitBash - https://git-scm.com/downloads

3. Clone o projeto para a sua máquina:
```git clone https://gitlab.com/projeto-de-desenvolvimento/estatecare.git```

4. Navegue até a pasta raíz que foi clonada

5. Execute o comando ```composer install``` a fim de adquirir todos os pacotes utilizados no projeto

6. Dentro do terminal execute o comando ```php artisan serve``` que deverá iniciar a execução do servidor(obs: se você fechar seu terminal que está rodando este comando a aplicação irá parar de funcionar)

8. Pronto, o projeto já deve estar disponível para você navegar e desenvolver na sua máquina, basta acessar a url ```http://localhost:8000/home``` no seu navegador


## Instruções para testes
A fim de possibilitar maior interação com o _software_ desenvolvido, utilizamos o serviço de hospedagem heroku, o qual pode ser acessado de acordo com as informações a seguir:

Para encontrar a plataforma, acesse o link http://estate-care.herokuapp.com/


**Perfil de Administrador:**

email: teste@admin.com

senha: 123456789

**Perfil de Super Administrador**

email: teste@super-admin.com

senha: 123456789



